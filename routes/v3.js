/**
 * Description
 *
 * Creation Date 12/10/2017
 * @author Candy
 */
'use strict'

import express from 'express';
import Banner from '../controller/v3/bannerData';

const router = express.Router();

router.get('/banners', Banner.getBanner);

export default router;