/**
 * Description
 *
 * Creation Date 24/10/2017
 * @author Candy
 */
'use strict'

import express from 'express';
import Captchas from '../controller/v1/captchas';
import User from '../controller/v2/user'
import Address from '../controller/v1/address'
import Cart from '../controller/v1/cart'
import Order from '../controller/v1/order'

const router = express.Router();

router.post('/captchas', Captchas.getCaptchas);
router.get('/user', User.getInfo);
router.get('/user/list', User.getUserList);
router.get('/user/:user_id', User.getInfoById);
router.post('/login', User.login);
router.get('/signout', User.signout);
router.post('/users/:user_id/addresses', Address.addAddress);
router.get('/users/:user_id/addresses', Address.getAddress);
router.get('/users/:user_id/addresses/:address_id', Address.getAddressById);
router.delete('/users/:user_id/addresses/:address_id', Address.deleteAddress);
router.put('/users/:user_id/addresses/:address_id/new', Address.updateAddress);
router.post('/carts', Cart.checkout)
router.get('/carts', Cart.getCartById)
router.delete('/carts', Cart.deleteCart)
router.get('/carts/list', Cart.getCarts)
router.post('/orders', Order.addOrder)
router.get('/orders', Order.getOrderById)
router.get('/orders/list', Order.getOrdersList)

export default router;
