/**
 * Category Router
 *
 * Creation Date 12/10/2017
 * @author Candy
 */
'use strict'

import express from 'express';
import Category from '../controller/category/category';

const router = express.Router();

router.get('/', Category.getCategoryByType);

export default router;