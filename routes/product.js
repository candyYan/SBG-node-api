/**
 * Description
 *
 * Creation Date 12/10/2017
 * @author Candy
 */
'use strict'

import express from 'express';
import Product from '../controller/product/product';

const router = express.Router();

router.get('/list', Product.getProduct);
router.get('/:product_id', Product.getProdutById);
router.get('/colors/:color_code_id', Product.getSingleProductPriceAndSize);

export default router;