/**
 * Router execute
 */

'use strict';
import category from './category';
import product from './product';
import v1 from './v1';
import v2 from './v2';
import v3 from './v3';

export default app => {
  app.use('/category', category);
  app.use('/product', product);
  app.use('/v1', v1);
  app.use('/v2', v2);
  app.use('/v3', v3);
}