/**
 * Description
 *
 * Creation Date 12/10/2017
 * @author Candy
 */
'use strict'

import express from 'express';
import Admin from '../controller/v2/admin'

const router = express.Router();

// router.get('/list', Product.getProduct);
router.post('/login', Admin.login);
// router.post('/register', Admin.register);
router.get('/singout', Admin.singout);
router.get('/all', Admin.getAllAdmin);
router.get('/count', Admin.getAdminCount);
router.get('/info', Admin.getAdminInfo);

export default router;