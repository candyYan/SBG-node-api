/**
 * Description
 *
 * Creation Date 13/10/2017
 * @author Candy
 */
'use strict'
import mongoose from 'mongoose'

const statisSchema = new mongoose.Schema({
  date: String,
  origin: String,
  statis_id: Number
})

statisSchema.index({statis_id: 1})

const statis = mongoose.model('statis', statisSchema);

export default statis
