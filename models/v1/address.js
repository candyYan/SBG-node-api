/**
 * Description
 *
 * Creation Date 24/10/2017
 * @author Candy
 */
import mongoose from 'mongoose'

const addressSchema = new mongoose.Schema({
	id: Number,
    user_id: Number,
	name: String,
	gender: String,
	mobile: String,
	email: String,
	address: String,
	is_active: Boolean
})

const Address = mongoose.model('Address', addressSchema)

export default Address