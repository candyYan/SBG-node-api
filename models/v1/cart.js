/**
 * Description
 *
 * Creation Date 13/10/2017
 * @author Candy
 */
import mongoose from 'mongoose'

const cartSchema = new mongoose.Schema({
    user_id: Number,
    cart_id: Number,
    cart: {
        groups: [
            {
                attrs: [],
                extra: [],
                product_id: Number,
                name: String,
                image: String,
                price: Number,
                quantity: Number
            }
        ],
        extras: Object,
        total_price: Number,
        payments: [
            {
                is_online_payment:  {type: Boolean, default: true}
            }
        ],
        address: Object,
        time: String
    }
})

cartSchema.index({id: 1})

const Cart = mongoose.model('Cart', cartSchema);

export default Cart