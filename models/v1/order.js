/**
 * Description
 *
 * Creation Date 27/10/2017
 * @author Candy
 */
'use strict'

import mongoose from 'mongoose'

const orderSchema = new mongoose.Schema({
    id: Number,
    user_id: Number,
    order_id: String,
    total: String,
    order_time: String,
    remark: String,
    is_payment: false,
    cart: {
        cart_id: Number,
        groups: []
    },
    address: Object
})

const Order = mongoose.model('Order', orderSchema)

export default Order