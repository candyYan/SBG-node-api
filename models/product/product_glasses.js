/**
 * Product Glasses Model
 *
 * Creation Date 12/10/2017
 * @author Candy
 */
'use strict';

import mongoose from 'mongoose';
import productGlassesData from '../../initData/sbg_product_glasses'

const productGlassesSchema = new mongoose.Schema({
  product_id: Number,
  category_id: Number,
  brand_id: Number,
  product_name: String,
  gender_id: Number,
  price: Number,
  color: String,
  product_image: String
});

const product_glasses = mongoose.model('product_glasses', productGlassesSchema);

product_glasses.findOne((err, data) => {
  if(!data) {
    productGlassesData.forEach(item => {
      product_glasses.create(item);
    })
  }
});

export default product_glasses;
