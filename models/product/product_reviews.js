/**
 * Description
 *
 * Creation Date 13/10/2017
 * @author Candy
 */
'use strict'
import mongoose from 'mongoose';
import productReviewsData from '../../initData/sbg_product_reviews'

const productReviewSchema = new mongoose.Schema({
  review_id: Number,
  server_id: Number,
  member_name: String,
  billing_country: String,
  product_id: Number,
  color_code_id: Number,
  category_id: Number,
  member_id: Number,
  status: Number,
  create_date: String,
  verified_status: Number,
  overall_rating: Number,
  style_rating: Number,
  comfort_rating: Number,
  average_rating: Number,
  helpful: Number,
  review_source: String,
  order_id: String,
  product_comments: String,
  service_comments: String,
  moderation_user: String,
  moderation_date: String,
  stransfer_status: Number
});

const product_review = mongoose.model('product_review', productReviewSchema);

product_review.findOne((err, data) => {
  if(!data) {
    productReviewsData.forEach(item => {
      product_review.create(item)
    })
  }
});

export default product_review;

