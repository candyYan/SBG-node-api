/**
 * Product Glasses Model
 *
 * Creation Date 12/10/2017
 * @author Candy
 */
'use strict';

import mongoose from 'mongoose';
import productGlassesColorData from '../../initData/sbg_product_glasses_color'

const productGlassesColorSchema = new mongoose.Schema({
  color_code_id: Number,
  gproduct_id: Number,
  product_name: String,
  color_code: String,
  small_image: String,
  big_image: String,
  degree_360: Number,
  frame_color_name: String,
  lens_color_name: String,
  try_flag: Number,
  stock_status: Number,
  is_active: Number,
  clearance_flag: Number,
  issue_year: Number,
  category_display: Number,
  polarized: Number,
  video_360: Number,
  is_twoforone: Number,
  has_stock: Number,
  asian_fit: Number,
  image_flag: Number,
  youtube_id: Number,
  folding: Number,
  mirror: Number,
  timestamp: String,
  front_facing: Number,
  replace_times: String,
  replace_date: String,
  flash_sale: Number,
  mirrored_lens: Number
});

const product_glasses_color = mongoose.model('product_glasses_color', productGlassesColorSchema);

product_glasses_color.findOne((err, data) => {
  if(!data) {
    productGlassesColorData.forEach(item => {
      product_glasses_color.create(item);
    })
  }
});

export default product_glasses_color;
