/**
 * Product Size Model
 *
 * Creation Date 13/10/2017
 * @author Candy
 */

'use strict';
import mongoose from 'mongoose';
import productSizeData from '../../initData/sbg_product_glasses_size'

const productSizeSchema = new mongoose.Schema({
  glasses_size_id: Number,
  gproduct_id: Number,
  color_code_id: Number,
  lens_width: Number,
  bridge_width: Number,
  lens_height: Number,
  temple_length: Number,
  upc: String,
  try_on_model: String,
  filter_size: Number,
  timestamp: String,
  rxable: Number,
  is_active: Number
});

const product_size = mongoose.model('product_size', productSizeSchema);

product_size.findOne((err, data) => {
  if(!data) {
    productSizeData.forEach(item => {
      product_size.create(item)
    })
  }
});

export default product_size;


