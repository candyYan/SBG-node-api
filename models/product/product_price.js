/**
 * Description
 *
 * Creation Date 13/10/2017
 * @author Candy
 */

'use strict';
import mongoose from 'mongoose';
import productPriceData from '../../initData/sbg_glasses_price_1'

const productPriceSchema = new mongoose.Schema({
  glasses_price_id: Number,
  color_code_id: Number,
  recom_retail_price: Number,
  selling_price: Number,
  origin_price: Number,
  timestamp: String
});

const product_price = mongoose.model('product_price', productPriceSchema);

product_price.findOne((err, data) => {
  if(!data) {
    productPriceData.forEach(item => {
      product_price.create(item)
    })
  }
});

export default product_price;