/**
 * Description
 *
 * Creation Date 13/10/2017
 * @author Candy
 */
import mongoose from  'mongoose'
import bannerData from '../../initData/bannerData'

const bannerSchema = new mongoose.Schema({
    banner_id: Number,
    banner_url: String,
    banner_link: String,
    banner_time: String
})
const Banner = mongoose.model('Banner', bannerSchema)

Banner.findOne((err, data) => {
    if(!data) {
        bannerData.forEach(item => {
            Banner.create(item)
        })
    }
})

export default Banner