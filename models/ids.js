/**
 * Ids Model
 *
 * Creation Date 12/10/2017
 * @author Candy
 */

'use strict';
import mongoose from 'mongoose';

const idSchema = new mongoose.Schema({
  category_id: Number,
  frame_type_id: Number,
  gender_id: Number,
  product_id: Number,
  color_code_id: Number,
  brand_id: Number,
  cart_id: Number,
  address_id: Number,
  order_id: Number,
  user_id: Number,
  admin_id: Number,
  statis_id: Number,
});

const Ids = mongoose.model('Ids', idSchema);

Ids.findOne((err, data) => {
  if(!data) {
    Ids.create({
      category_id: 21,
      frame_type_id: 12,
      gender_id: 4,
      product_id: 70,
      color_code_id: 343,
      brand_id: 71,
      cart_id: 0,
      address_id: 0,
      order_id: 0,
      user_id: 0,
      admin_id: 0,
      statis_id: 0,
    })
  }
});

export default Ids;

