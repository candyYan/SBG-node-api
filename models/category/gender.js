/**
 * Product Category Model
 *
 * Creation Date 12/10/2017
 * @author Candy
 */

'use strict';
import mongoose from 'mongoose';
import genderData from '../../initData/sbg_gender';

const genderSchema = new mongoose.Schema({
  gender_id: Number,
  gender_name: String
});

const gender = mongoose.model('gender', genderSchema);

gender.findOne((err, data) => {
  if(!data) {
    genderData.forEach(item => {
      gender.create(item);
    })
  }
});

export default gender;
