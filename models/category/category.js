/**
 * Product Category Model
 *
 * Creation Date 12/10/2017
 * @author Candy
 */

'use strict';
import mongoose from 'mongoose';
import categoryData from '../../initData/sbg_category';

const categorySchema = new mongoose.Schema({
  category_id: Number,
  category_name: String,
  parent_id: Number,
  var_category_name: String,
  category_url: String,
  duration: String
});

const category = mongoose.model('category', categorySchema);

category.findOne((err, data) => {
  if(!data) {
    categoryData.forEach(item => {
      category.create(item);
    })
  }
});

export default category;
