/**
 * Description
 *
 * Creation Date 12/10/2017
 * @author Candy
 */

'use strict';
import mongoose from 'mongoose';
import brandData from '../../initData/sbg_brand';

const brandSchema = new mongoose.Schema({
  brand_id: Number,
  brand_name: String,
  sunglass_flag: Number,
  eyeglass_flag: Number,
  clens_flag: Number,
  brand_image: String,
  manufacturer: String,
  show_flag: Number,
  primary_supplier_id: Number,
  brand_group: Number,
  menu_new_flag: Number,
  menu_logo_flag: Number,
  manufacturer_image: String,
  eye_package_image: String,
  sun_package_image: String,
  house_brand: Number,
  is_active: Number,
  is_consider_lux_region: String
});

const brand = mongoose.model('brand', brandSchema);

brand.findOne((err, data) => {
  if(!data) {
    brandData.forEach(item => {
      brand.create(item);
    })
  }
});

export default brand;
