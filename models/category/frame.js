/**
 * Product Category Model
 *
 * Creation Date 12/10/2017
 * @author Candy
 */

'use strict';
import mongoose from 'mongoose';
import frameData from '../../initData/sbg_frame_shape';

const frameSchema = new mongoose.Schema({
  frame_shape_id: Number,
  frame_shape_name: String
});

const frame = mongoose.model('frame', frameSchema);

frame.findOne((err, data) => {
  if(!data) {
    frameData.forEach(item => {
      frame.create(item);
    })
  }
});

export default frame;
