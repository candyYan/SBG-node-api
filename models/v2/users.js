/**
 * Description
 *
 * Creation Date 13/10/2017
 * @author Candy
 */
import mongoose from  'mongoose'

const userSchema = new mongoose.Schema({
    user_id: Number,
    username: String,
    password: String,
})
const User = mongoose.model('User', userSchema)

export default User