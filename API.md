# node-elm 接口文档
```

域名: http://47.100.6.170

端口：3389

```

## 目录：

[1、获取首页banners](#1获取首页banners)<br/>
[2、获取分类列表](#2获取分类列表)<br/>
[3、获取眼镜列表](#3获取眼镜列表)<br/>
[4、眼镜产品信息详细](#4眼镜产品信息详细)<br/>
[5、获取验证码](#5获取验证码)<br/>
[6、登录](#6登录)<br/>
[7、退出登录](#7退出登录)<br/>
[8、获取用户信息](#8获取用户信息)<br/>
[9、获取所有用户列表](#9获取所有用户列表)<br/>
[10、获取所有管理员列表](#9获取所有管理员列表)<br/>
[11、获取所有管理员数量](#10获取所有管理员数量)<br/>
[12、管理员登录](#12管理员登录)<br/>
[13、管理员退出登录](#13管理员退出登录)<br/>
[14、获取当前用户地址列表](#14获取当前用户地址列表)<br/>
[15、获取当前用户地址](#15获取当前用户地址)<br/>
[16、添加当前用户地址](#16添加当前用户地址)<br/>
[17、更新当前用户地址](#17更新当前用户地址)<br/>
[18、删除当前用户地址](#18删除当前用户地址)<br/>
[19、结算](#19结算)<br/>
[20、获取购物车列表](#20获取购物车列表)<br/>
[21、获取购物车信息](#21获取购物车信息)<br/>
[22、删除购物车信息](#22删除购物车信息)<br/>
[23、创建订单](#23创建订单)<br/>
[24、获取订单列表](#24获取订单列表)<br/>
[25、获取订单信息](#25获取订单信息)<br/>



## 接口列表：

### 1、获取首页banners

#### 请求URL:  
```
http://47.100.6.170:3389/v3/banners
```

#### 示例：
 [http://47.100.6.170:3389/v3/banners](http://47.100.6.170:3389/v3/banners)

#### 请求方式: 
```
GET
```

#### 参数类型：query

|参数|是否必选|类型|说明|
|:-----|:-------:|:-----|:-----|

#### 返回示例：

```javascript
[
    {"_id":"5a387a0016c346435934937d","banner_id":1,"banner_url":"https://84654884b68f749e7aa3-aa57143e2ca30795bf94ff3884a3b19b.ssl.cf1.rackcdn.com/en/images/banners/ImgSlider-Mobile-CL-EN.jpg","banner_link":"https://smartbuyglasses.tmall.hk/","banner_time":"2017-12-14 24:59:59","__v":0},
    {"_id":"5a387a0016c346435934937e","banner_id":2,"banner_url":"https://84654884b68f749e7aa3-aa57143e2ca30795bf94ff3884a3b19b.ssl.cf1.rackcdn.com/en/images/banners/HP-Mobile-NH-11.jpg","banner_link":"https://smartbuyglasses.tmall.hk/category-1307936441.htm?spm=a1z10.1-b-s.w5003-16486414935.36.ws74Hp&search=y&tsearch=y&scene=taobao_shop#TmshopSrchNav","banner_time":"2017-12-14 24:59:59","__v":0}
]
```

### 2、获取分类列表

#### 请求URL:  
```
http://47.100.6.170:3389/category
```

#### 示例：
 [http://47.100.6.170:3389/category?type=brand](http://47.100.6.170:3389/category?type=brand)

#### 请求方式: 
```
GET
```

#### 参数类型：query

|参数|是否必选|类型|说明|
|:-----|:-------:|:-----|:-----|
|type      |Y       |string  |category：眼镜类型分类，  gender：性别分类， brand：品牌分类 |

#### 返回示例：

```javascript
[
    {"brand_id":3,"brand_name":"Bvlgari","brand_image":"https://84654884b68f749e7aa3-aa57143e2ca30795bf94ff3884a3b19b.ssl.cf1.rackcdn.com/public/images/showbrand/brand_Bvlgari.gif","__v":0},
    {"brand_id":13,"brand_name":"Prada","brand_image":"https://84654884b68f749e7aa3-aa57143e2ca30795bf94ff3884a3b19b.ssl.cf1.rackcdn.com/public/images/showbrand/brand_Prada.gif","__v":0},
    {"brand_id":64,"brand_name":"Porsche Design","brand_image":"https://84654884b68f749e7aa3-aa57143e2ca30795bf94ff3884a3b19b.ssl.cf1.rackcdn.com/public/images/showbrand/brand_Porsche%20Design.gif","__v":0},
    {"brand_id":7,"brand_name":"Dolce & Gabbana","brand_image":"https://84654884b68f749e7aa3-aa57143e2ca30795bf94ff3884a3b19b.ssl.cf1.rackcdn.com/public/images/showbrand/brand_Dolce%20%26%20Gabbana.gif","__v":0},
    {"brand_id":25,"brand_name":"Police","brand_image":"https://84654884b68f749e7aa3-aa57143e2ca30795bf94ff3884a3b19b.ssl.cf1.rackcdn.com/public/images/showbrand/brand_Police.gif","__v":0},
    {"brand_id":12,"brand_name":"Miu Miu","brand_image":"https://84654884b68f749e7aa3-aa57143e2ca30795bf94ff3884a3b19b.ssl.cf1.rackcdn.com/public/images/showbrand/brand_Miu%20Miu.gif","__v":0},
    {"brand_id":14,"brand_name":"Ray Ban","brand_image":"https://84654884b68f749e7aa3-aa57143e2ca30795bf94ff3884a3b19b.ssl.cf1.rackcdn.com/public/images/showbrand/brand_Ray%20Ban.gif","__v":0},
    {"brand_id":2,"brand_name":"Burberry","brand_image":"https://84654884b68f749e7aa3-aa57143e2ca30795bf94ff3884a3b19b.ssl.cf1.rackcdn.com/public/images/showbrand/brand_Burberry.gif","__v":0},
    {"brand_id":10,"brand_name":"Gucci","brand_image":"https://84654884b68f749e7aa3-aa57143e2ca30795bf94ff3884a3b19b.ssl.cf1.rackcdn.com/public/images/showbrand/brand_Gucci.gif","__v":0},
    {"brand_id":21,"brand_name":"Adidas","brand_image":"https://84654884b68f749e7aa3-aa57143e2ca30795bf94ff3884a3b19b.ssl.cf1.rackcdn.com/public/images/showbrand/brand_Adidas.gif","__v":0},
    {"brand_id":17,"brand_name":"Versace","brand_image":"https://84654884b68f749e7aa3-aa57143e2ca30795bf94ff3884a3b19b.ssl.cf1.rackcdn.com/public/images/showbrand/brand_Versace.gif","__v":0},
    {"brand_id":58,"brand_name":"Tom Ford","brand_image":"https://84654884b68f749e7aa3-aa57143e2ca30795bf94ff3884a3b19b.ssl.cf1.rackcdn.com/public/images/showbrand/brand_Tom%20Ford.gif","__v":0}
]
```

### 3、获取眼镜列表

#### 请求URL:  
```
http://47.100.6.170:3389/product/list
```

#### 示例：
 [http://47.100.6.170:3389/product/list?brand_id=14](http://47.100.6.170:3389/product/list?brand_id=14)

#### 请求方式: 
```
GET
```

#### 参数类型：query

|参数|是否必选|类型|说明|
|:-----|:-------:|:-----|:-----|
|keyword      |N       |string  |关键词 |
|brand_id      |N       |number  |品牌id |
|category_id      |N       |number  |眼镜类型id |
|gender_id      |N       |number  |性别类型id |
|sort      |N       |number  | 0： 价格从高到底排序， 1：价格从低到高排序 |

#### 返回示例：

```javascript
[
    {"product_id":1,"category_id":1,"brand_id":14,"gender_id":2,"product_name":"RB4165 Justin 852/88","price":700,"product_image":"HPbanner/sunglasses/Ray%20Ban%20RB%204165%20852_88.jpg","__v":0},
    {"product_id":4,"category_id":2,"brand_id":14,"gender_id":3,"product_name":"RX5228 Highstreet 2000","price":801,"product_image":"HPbanner/glasses/Ray_Ban_RB5228_2000.jpg","__v":0}
]
```
### 4、眼镜产品信息详细

#### 请求URL:  
```
http://47.100.6.170:3389/product/:product_id
```

#### 示例：
 [http://47.100.6.170:3389/product/1](http://47.100.6.170:3389/product/1)

#### 请求方式: 
```
GET
```

#### 参数类型：param

|参数|是否必选|类型|说明|
|:-----|:-------:|:-----|:-----|
|product_id      |Y       |int   |产品id |

#### 返回示例：

```javascript

{"product_id":1,"category_id":1,"brand_id":14,"gender_id":2,"product_name":"RB4165 Justin 852/88","price":700,"product_image":"HPbanner/sunglasses/Ray%20Ban%20RB%204165%20852_88.jpg","__v":0}

```

### 5、获取验证码

#### 请求URL:  
```
http://47.100.6.170:3389/v1/captchas
```

#### 示例：
 [http://47.100.6.170:3389/v1/captchas](http://47.100.6.170:3389/v1/captchas)

#### 请求方式: 
```
post
```

#### 参数类型：param

|参数|是否必选|类型|说明|
|:-----|:-------:|:-----|:-----|

#### 返回示例：

```javascript
{
    "status":1,
    "code":"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAAAeCAMAAACMnWmDAAAAGFBMVEUAAABQUFAAAAAAAAAAAAAAAAAAAAAAAABiRp8mAAAACHRSTlMA/wAAAAAAACXRGJEAAAmJSURBVHjaAX4JgfYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEBAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAQEBAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQEBAQEBAQAAAAAAAAAAAAAAAAAAAAEBAQEBAQEBAQEBAQEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQEBAQEBAQAAAAAAAAAAAAAAAAAAAAEBAQEBAQEBAQEBAQEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQEBAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEBAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQEBAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQEBAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEBAQEBAAAAAAAAAAAAAAAAAQEBAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAQEAAAAAAAAAAAAAAAAAAAAAAAEBAQEBAQAAAAAAAAAAAAABAQEBAQEBAQEAAAAAAAAAAAAAAQEBAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAQAAAAAAAAAAAAAAAAAAAAAAAQEBAQEBAQEAAAAAAAAAAAEBAQAAAAAAAQEAAAAAAAAAAAAAAQEBAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAEBAAAAAAAAAAAAAAAAAAAAAAABAQEBAQEBAQEBAAAAAAAAAAEBAAAAAAAAAAEBAAAAAAAAAAAAAQEBAQAAAAAAAAAAAAAAAAAAAAAAAAAAAQEBAAAAAAAAAAAAAAAAAAAAAAABAQEBAAABAQEBAAAAAAAAAQEAAAAAAAAAAAEBAAAAAAAAAAAAAQEBAQAAAAAAAAAAAAAAAAAAAAAAAAABAQEAAAAAAAAAAAAAAAAAAAAAAAABAQEBAAABAQEBAAAAAAAAAQEAAAAAAAAAAAEBAAAAAAAAAAAAAQEBAQAAAAAAAAAAAAAAAAAAAAAAAAABAQAAAAAAAAAAAAAAAAAAAAAAAAABAQEBAAABAQEBAAAAAAAAAQEAAAAAAAAAAAEBAAAAAAAAAAAAAQEBAQAAAAAAAAAAAAAAAAAAAAAAAAEBAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAQEBAAAAAAAAAQEAAAAAAAAAAQEBAAAAAAAAAAAAAQEBAQAAAAAAAAAAAAAAAAAAAAAAAQEBAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAQEBAQEAAAAAAAAAAAEBAAAAAAEBAQEAAAAAAAAAAAAAAQEBAQAAAAAAAAAAAAAAAAAAAAAAAQEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAQEBAQAAAAAAAAAAAAEBAQEBAQEBAQEAAAAAAAAAAAAAAQEBAQAAAAAAAAAAAAAAAAAAAAABAQEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAQEBAQEBAAAAAAAAAAAAAQEBAQAAAQEAAAAAAAAAAAAAAQEBAQAAAAAAAAAAAAAAAAAAAAEBAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAQEBAAAAAAAAAAAAAAAAAAABAQAAAAAAAAAAAAAAAQEBAQAAAAAAAAAAAAAAAAAAAAEBAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAQEBAAABAQEBAAAAAAAAAAAAAAAAAAEBAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEBAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAQEBAAABAQEBAAAAAAAAAAAAAAAAAQEBAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAQEBAAABAQEBAAAAAAAAAAAAAAEBAQEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAQEBAAABAQEBAAAAAAABAQEBAQEBAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAQEBAAABAQEBAAAAAAABAQEBAQEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAQEBAQEBAQEBAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQEBAQEBAQEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEBAQEBAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACVtwFx6wIFkAAAAABJRU5ErkJggg=="
}

```


### 6、登录

#### 请求URL:  
```
http://47.100.6.170:3389/v1/login
```

#### 示例：
 [http://47.100.6.170:3389/v1/login](http://47.100.6.170:3389/v1/login)

#### 请求方式: 
```
post
```

#### 参数类型：param

|参数|是否必选|类型|说明|
|:-----|:-------:|:-----|:-----|
|username      |Y       |string  |用户名 |
|password      |Y       |string  |密码 |
|cap      |Y       |string  |验证码（/v1/captchas 获取的验证码） |
|  |  | 未注册的用户会自动注册，并登录 |

#### 返回示例：

```javascript
{ 
    "username":"candy",
    "user_id":1,
    "id":1,
    "column_desc":{"gift_mall_desc":"0元好物在这里","game_link":"https://gamecenter.faas.ele.me","game_is_show":1,"game_image_hash":"05f108ca4e0c543488799f0c7c708cb1jpeg","game_desc":"玩游戏领红包"},
    "point":0,
    "mobile":"",
    "is_mobile_valid":true,
    "is_email_valid":false,
    "is_active":1,
    "gift_amount":3,"email":"",
    "delivery_card_expire_days":0,
    "current_invoice_id":0,
    "current_address_id":0,
    "brand_member_new":0,
    "balance":0,
    "avatar":"default.jpg",
    "__v":0 
}

```


### 7、退出登录

#### 请求URL:  
```
http://47.100.6.170:3389/v1/signout
```

#### 示例：
 [http://47.100.6.170:3389/v1/signout](http://47.100.6.170:3389/v1/signout)

#### 请求方式: 
```
get
```

#### 参数类型：query

|参数|是否必选|类型|说明|
|:-----|:-------:|:-----|:-----|

#### 返回示例：

```javascript
{
    "status":1,
    "message":"退出成功"
}

```
### 8、获取用户信息

#### 请求URL:  
```
http://47.100.6.170:3389/v1/user/:user_id
```

#### 示例：
 [http://47.100.6.170:3389/v1/user/1](http://47.100.6.170:3389/v1/user/1)

#### 请求方式: 
```
get
```

#### 参数类型：query

|参数|是否必选|类型|说明|
|:-----|:-------:|:-----|:-----|
|user_id      |Y       |number  |用户id |

#### 返回示例：

```javascript
{ 
    "username":"candy",
    "user_id":1,
    "id":1,
    "column_desc":{"gift_mall_desc":"0元好物在这里","game_link":"https://gamecenter.faas.ele.me","game_is_show":1,"game_image_hash":"05f108ca4e0c543488799f0c7c708cb1jpeg","game_desc":"玩游戏领红包"},
    "point":0,
    "mobile":"",
    "is_mobile_valid":true,
    "is_email_valid":false,
    "is_active":1,
    "gift_amount":3,"email":"",
    "delivery_card_expire_days":0,
    "current_invoice_id":0,
    "current_address_id":0,
    "brand_member_new":0,
    "balance":0,
    "avatar":"default.jpg",
    "__v":0 
}

```

### 9、获取所有用户列表

#### 请求URL:  
```
http://47.100.6.170:3389/v1/user/list
```

#### 示例：
 [http://47.100.6.170:3389/v1/user/list](http://47.100.6.170:3389/v1/user/list)

#### 请求方式: 
```
get
```

#### 参数类型：query

|参数|是否必选|类型|说明|
|:-----|:-------:|:-----|:-----|

#### 返回示例：

```javascript
[
    {"username":"candy","user_id":1,"id":1,"column_desc":{"gift_mall_desc":"0元好物在这里","game_link":"https://gamecenter.faas.ele.me","game_is_show":1,"game_image_hash":"05f108ca4e0c543488799f0c7c708cb1jpeg","game_desc":"玩游戏领红包"},"point":0,"mobile":"","is_mobile_valid":true,"is_email_valid":false,"is_active":1,"gift_amount":3,"email":"","delivery_card_expire_days":0,"current_invoice_id":0,"current_address_id":0,"brand_member_new":0,"balance":0,"avatar":"default.jpg","__v":0},
    {"username":"admin","user_id":2,"id":2,"column_desc":{"gift_mall_desc":"0元好物在这里","game_link":"https://gamecenter.faas.ele.me","game_is_show":1,"game_image_hash":"05f108ca4e0c543488799f0c7c708cb1jpeg","game_desc":"玩游戏领红包"},"point":0,"mobile":"","is_mobile_valid":true,"is_email_valid":false,"is_active":1,"gift_amount":3,"email":"","delivery_card_expire_days":0,"current_invoice_id":0,"current_address_id":0,"brand_member_new":0,"balance":0,"avatar":"default.jpg","__v":0}
]

```


### 10、获取所有管理员列表

#### 请求URL:  
```
http://47.100.6.170:3389/v2/all
```

#### 示例：
 [http://47.100.6.170:3389/v2/all](http://47.100.6.170:3389/v2/all)

#### 请求方式: 
```
get
```

#### 参数类型：query

|参数|是否必选|类型|说明|
|:-----|:-------:|:-----|:-----|

#### 返回示例：

```javascript
{
    "status":1,
    "data":[{"user_name":"admin","id":2,"create_time":"2018-01-10 12:03","status":1,"__v":0,"avatar":"default.jpg","admin":"管理员"},{"user_name":"candy","id":1,"create_time":"2018-01-08 17:50","status":1,"__v":0,"avatar":"default.jpg","admin":"管理员"}]
}

```

### 11、获取所有管理员数量

#### 请求URL:  
```
http://47.100.6.170:3389/v2/count
```

#### 示例：
 [http://47.100.6.170:3389/v2/count](http://47.100.6.170:3389/v2/count)

#### 请求方式: 
```
get
```

#### 参数类型：query

|参数|是否必选|类型|说明|
|:-----|:-------:|:-----|:-----|

#### 返回示例：

```javascript
{
    "status":1,
    "count":2
}

```


### 12、管理员登录

#### 请求URL:  
```
http://47.100.6.170:3389/v2/login
```

#### 示例：
 [http://47.100.6.170:3389/v2/login](http://47.100.6.170:3389/v2/login)

#### 请求方式: 
```
post
```

#### 参数类型：param

|参数|是否必选|类型|说明|
|:-----|:-------:|:-----|:-----|
|user_name      |Y       |string  |用户名 |
|password      |Y       |string  |密码 |
|  |  | 未注册的用户会自动注册，并登录 |

#### 返回示例：

```javascript
{
    "status":1,
    "success":"登录成功"
}
```


### 13、管理员退出登录

#### 请求URL:  
```
http://47.100.6.170:3389/v2/singout
```

#### 示例：
 [http://47.100.6.170:3389/v2/singout](http://47.100.6.170:3389/v2/singout)

#### 请求方式: 
```
get
```

#### 参数类型：query

|参数|是否必选|类型|说明|
|:-----|:-------:|:-----|:-----|

#### 返回示例：

```javascript
{
    "status":1,
    "success":"退出成功"
}
```

### 14、获取当前用户地址列表

#### 请求URL:  
```
http://47.100.6.170:3389/v1/users/:user_id/addresses
```

#### 示例：
 [http://47.100.6.170:3389/v1/users/2/addresses](http://47.100.6.170:3389/v1/users/2/addresses)

#### 请求方式: 
```
get
```

#### 参数类型：query

|参数|是否必选|类型|说明|
|:-----|:-------:|:-----|:-----|
|user_id      |Y       |number  |用户id |

#### 返回示例：

```javascript
[
    {"id":4,"user_id":2,"name":"candy","gender":"men","mobile":"12343","email":"ew","address":"ew","is_active":true,"__v":0},
    {"id":3,"user_id":2,"name":"admin","gender":"men","mobile":"127681902","email":"ga@dd.com","address":"admin","is_active":false,"__v":0},
    {"id":2,"user_id":2,"name":"candy","gender":"women","mobile":"167231452","email":"test@qq.com","address":"test","is_active":false,"__v":0}
]
```


### 15、获取当前用户地址

#### 请求URL:  
```
http://47.100.6.170:3389/v1/users/:user_id/addresses/:address_id
```

#### 示例：
 [http://47.100.6.170:3389/v1/users/2/addresses/3](http://47.100.6.170:3389/v1/users/2/addresses/3)

#### 请求方式: 
```
get
```

#### 参数类型：query

|参数|是否必选|类型|说明|
|:-----|:-------:|:-----|:-----|
|user_id      |Y       |number  |用户id |
|address_id      |Y       |number  |地址id |

#### 返回示例：

```javascript
{
    "id":3,
    "user_id":2,
    "name":"admin",
    "gender":"men",
    "mobile":"127681902",
    "email":"ga@dd.com",
    "address":"admin",
    "is_active":false,
    "__v":0
}
```


### 16、添加当前用户地址

#### 请求URL:  
```
http://47.100.6.170:3389/v1/users/:user_id/addresses
```

#### 示例：
 [http://47.100.6.170:3389/v1/users/2/addresses](http://47.100.6.170:3389/v1/users/2/addresses)

#### 请求方式: 
```
post
```

#### 参数类型：param

|参数|是否必选|类型|说明|
|:-----|:-------:|:-----|:-----|
|user_id      |Y       |number  |用户id |
|name      |Y       |string  |名字 |
|gender      |Y       |string  |性别 |
|mobile      |Y       |string  |手机 |
|email      |Y       |string  |邮箱 |
|address      |Y       |string  |详细地址 |
|is_active      |Y       |boolean  |true: 设为默认地址, false：不设为默认地址 |

#### 返回示例：

```javascript
{
    "status":1,
    "message":"Add address successful!"
}
```

### 17、更新当前用户地址

#### 请求URL:  
```
http://47.100.6.170:3389/v1/users/:user_id/addresses/:address_id/new
```

#### 示例：
 [http://47.100.6.170:3389/v1/users/2/addresses/3/new](http://47.100.6.170:3389/v1/users/2/addresses/3/new)

#### 请求方式: 
```
post
```

#### 参数类型：param

|参数|是否必选|类型|说明|
|:-----|:-------:|:-----|:-----|
|user_id      |Y       |number  |用户id |
|name      |Y       |string  |名字 |
|gender      |Y       |string  |性别 |
|mobile      |Y       |string  |手机 |
|email      |Y       |string  |邮箱 |
|address      |Y       |string  |详细地址 |
|is_active      |Y       |boolean  |true: 设为默认地址, false：不设为默认地址 |

#### 返回示例：

```javascript
{
    "status":1,
    "message":"Add address successful!"
}
```


### 18、删除当前用户地址

#### 请求URL:  
```
http://47.100.6.170:3389/v1/users/:user_id/addresses/:address_id
```

#### 示例：
 [http://47.100.6.170:3389/v1/users/2/addresses/4](http://47.100.6.170:3389/v1/users/2/addresses/4)

#### 请求方式: 
```
delete
```

#### 参数类型：param

|参数|是否必选|类型|说明|
|:-----|:-------:|:-----|:-----|
|user_id      |Y       |number  |用户id |
|address_id      |Y       |string  |地址id |

#### 返回示例：

```javascript
{
    "status":1,
    "message":"Delete Address Successful!"
}
```


### 19、结算

#### 请求URL:  
```
http://47.100.6.170:3389/v1/carts
```

#### 示例：
 [http://47.100.6.170:3389/v1/carts?user_id=2](http://47.100.6.170:3389/v1/carts?user_id=2)

#### 请求方式: 
```
post
```

#### 参数类型：query

|参数|是否必选|类型|说明|
|:-----|:-------:|:-----|:-----|
|user_id      |Y       |number  |用户id |

#### 参数类型：param

|参数|是否必选|类型|说明|
|:-----|:-------:|:-----|:-----|
|entities      |Y       |array  |购物车数据[{attrs:[],extra:{},product_id:产品id,name:名称,image:图片,price:价格,quantity:数量}] |

#### 返回示例：

```javascript
{
    "__v":0,
    "user_id":2,
    "cart_id":5,
     "_id":"5a559c064d649a443e1219b7",
     "cart":{
        "extras":[{"price":10,"name":"运费","description":""}],
        "total_price":2316,
        "address":{"id":3,"user_id":2,"name":"admin","gender":"men","mobile":"127681902","email":"ga@dd.com","address":"admin","is_active":true,"__v":0},
        "time":"2018-01-10 12:52",
        "payments":[{"_id":"5a559c064d649a443e1219b8","is_online_payment":false}],
        "groups":[
            {
                "product_id":3,
                "name":"DG6086 Over-Molded Rubber Polarized 2809T3",
                "image":"HPbanner/sunglasses/Dolce%20%26%20Gabbana%20DG6086%20Over-Molded%20Rubber%20Polarized_2809T3.jpg",
                "price":1153,
                "quantity":2,
                "_id":"5a559c064d649a443e1219b9",
                "extra":[],
                "attrs":[]
            }
        ]
    }
}
```

### 20、获取购物车列表

#### 请求URL:  
```
http://47.100.6.170:3389/v1/carts/list
```

#### 示例：
 [http://47.100.6.170:3389/v1/carts/list?user_id=2&cart_id=5](http://47.100.6.170:3389/v1/carts/list?user_id=2&cart_id=5)

#### 请求方式: 
```
get
```

#### 参数类型：query

|参数|是否必选|类型|说明|
|:-----|:-------:|:-----|:-----|
|user_id      |Y       |number  |用户id |
|cart_id      |Y       |number  |购物车id |

#### 返回示例：

```javascript
[
    {"user_id":2,"cart_id":1,"cart":{"extras":[{"description":"","name":"运费","price":10}],"total_price":1410,"address":null,"time":"2018-01-02 11:39","payments":[{"_id":"5a4aff0b4d649a443e120b83","is_online_payment":false}],"groups":[{"product_id":1,"name":"RB4165 Justin 852/88","image":"HPbanner/sunglasses/Ray%20Ban%20RB%204165%20852_88.jpg","price":700,"quantity":2,"_id":"5a4aff0b4d649a443e120b84","extra":[],"attrs":[]}]},"__v":0},
    {"user_id":2,"cart_id":2,"cart":{"extras":[{"description":"","name":"运费","price":10}],"total_price":710,"address":null,"time":"2018-01-02 11:40","payments":[{"_id":"5a4aff214d649a443e120b99","is_online_payment":false}],"groups":[{"product_id":1,"name":"RB4165 Justin 852/88","image":"HPbanner/sunglasses/Ray%20Ban%20RB%204165%20852_88.jpg","price":700,"quantity":1,"_id":"5a4aff214d649a443e120b9a","extra":[],"attrs":[]}]},"__v":0}
]
```


### 21、获取购物车信息

#### 请求URL:  
```
http://47.100.6.170:3389/v1/carts
```

#### 示例：
 [http://47.100.6.170:3389/v1/carts?user_id=2&cart_id=5](http://47.100.6.170:3389/v1/carts?user_id=2&cart_id=5)

#### 请求方式: 
```
get
```

#### 参数类型：query

|参数|是否必选|类型|说明|
|:-----|:-------:|:-----|:-----|
|user_id      |Y       |number  |用户id |
|cart_id      |Y       |number  |购物车id |

#### 返回示例：

```javascript
{
    "user_id":2,
    "cart_id":5,
    "cart":{
        "extras":[{"description":"","name":"运费","price":10}],
        "total_price":2316,
        "address":{"__v":0,"is_active":true,"address":"admin","email":"ga@dd.com","mobile":"127681902","gender":"men","name":"admin","user_id":2,"id":3},
        "time":"2018-01-10 12:52",
        "payments":[{"_id":"5a559c064d649a443e1219b8","is_online_payment":false}],
        "groups":[
            {
                "product_id":3,
                "name":"DG6086 Over-Molded Rubber Polarized 2809T3",
                "image":"HPbanner/sunglasses/Dolce%20%26%20Gabbana%20DG6086%20Over-Molded%20Rubber%20Polarized_2809T3.jpg",
                "price":1153,
                "quantity":2,
                "_id":"5a559c064d649a443e1219b9",
                "extra":[],
                "attrs":[]
            }
        ]
    },
    "__v":0
}
```

### 22、删除购物车信息

#### 请求URL:  
```
http://47.100.6.170:3389/v1/carts
```

#### 示例：
 [http://47.100.6.170:3389/v1/carts?user_id=2&cart_id=5](http://47.100.6.170:3389/v1/carts?user_id=2&cart_id=5)

#### 请求方式: 
```
delete
```

#### 参数类型：query

|参数|是否必选|类型|说明|
|:-----|:-------:|:-----|:-----|
|user_id      |Y       |number  |用户id |
|cart_id      |Y       |number  |购物车id |

#### 返回示例：

```javascript
{
    status: 1,
    message: 'Delete Cart successful'
}
```



### 23、创建订单

#### 请求URL:  
```
http://47.100.6.170:3389/v1/orders
```

#### 示例：
 [http://47.100.6.170:3389/v1/orders](http://47.100.6.170:3389/v1/orders)

#### 请求方式: 
```
post
```

#### 参数类型：param

|参数|是否必选|类型|说明|
|:-----|:-------:|:-----|:-----|
|total      |Y       |number  |总价 |
|cart      |Y       |object  |购物车 {cart_id:购物车id, groups:[{attrs:[],extra:{},product_id:产品id,name:名称,image:图片,price:价格,quantity:数量}]} |
|address      |Y       |object  |地址 {{ id:地址id, user_id:用户id, name:用户名字, gender:用户性别,mobile:手机,address详细地址:}}|

#### 返回示例：

```javascript
{
    "id":6,
    "status":1,
    "message":"Order is successful"
}
```

### 24、获取订单列表

#### 请求URL:  
```
http://47.100.6.170:3389/v1/orders/list
```

#### 示例：
 [http://47.100.6.170:3389/v1/orders/list?user_id=2](http://47.100.6.170:3389/v1/orders/list?user_id=2)

#### 请求方式: 
```
get
```

#### 参数类型：query

|参数|是否必选|类型|说明|
|:-----|:-------:|:-----|:-----|
|user_id      |Y       |number  |用户id |

#### 返回示例：

```javascript
[
    {"id":6,"user_id":2,"order_id":"CN1515564950885","total":"2316","order_time":"2018-01-10 14:15","remark":"remark","address":{"__v":0,"is_active":true,"address":"admin","email":"ga@dd.com","mobile":"127681902","gender":"men","name":"admin","user_id":2,"id":3},"cart":{"cart_id":5,"groups":[{"product_id":3,"name":"DG6086 Over-Molded Rubber Polarized 2809T3","image":"HPbanner/sunglasses/Dolce%20%26%20Gabbana%20DG6086%20Over-Molded%20Rubber%20Polarized_2809T3.jpg","price":1153,"quantity":2,"_id":"5a559c064d649a443e1219b9","extra":[],"attrs":[]}]},"__v":0},
    {"id":5,"user_id":2,"order_id":"CN1515564908526","total":"2316","order_time":"2018-01-10 14:15","remark":"remark","cart":{"cart_id":5,"groups":[{"product_id":3,"name":"DG6086 Over-Molded Rubber Polarized 2809T3","image":"HPbanner/sunglasses/Dolce%20%26%20Gabbana%20DG6086%20Over-Molded%20Rubber%20Polarized_2809T3.jpg","price":1153,"quantity":2,"_id":"5a559c064d649a443e1219b9","extra":[],"attrs":[]}]},"__v":0},
    {"id":4,"user_id":2,"order_id":"CN1515564886470","total":"2316","order_time":"2018-01-10 14:14","remark":"remark","cart":{"cart_id":5,"groups":[{"product_id":3,"name":"DG6086 Over-Molded Rubber Polarized 2809T3","image":"HPbanner/sunglasses/Dolce%20%26%20Gabbana%20DG6086%20Over-Molded%20Rubber%20Polarized_2809T3.jpg","price":1153,"quantity":2,"_id":"5a559c064d649a443e1219b9","extra":[],"attrs":[]}]},"__v":0},
    {"id":3,"user_id":2,"order_id":"CN1515564606223","total":"2316","order_time":"2018-01-10 14:10","remark":"remark","cart":{"cart_id":5,"groups":[{"product_id":3,"name":"DG6086 Over-Molded Rubber Polarized 2809T3","image":"HPbanner/sunglasses/Dolce%20%26%20Gabbana%20DG6086%20Over-Molded%20Rubber%20Polarized_2809T3.jpg","price":1153,"quantity":2,"_id":"5a559c064d649a443e1219b9","extra":[],"attrs":[]}]},"__v":0},
    {"id":2,"user_id":2,"order_id":"CN1515564446891","total":"2316","order_time":"2018-01-10 14:07","remark":"remark","cart":{"cart_id":5,"groups":[{"product_id":3,"name":"DG6086 Over-Molded Rubber Polarized 2809T3","image":"HPbanner/sunglasses/Dolce%20%26%20Gabbana%20DG6086%20Over-Molded%20Rubber%20Polarized_2809T3.jpg","price":1153,"quantity":2,"_id":"5a559c064d649a443e1219b9","extra":[],"attrs":[]}]},"__v":0},
    {"id":1,"user_id":2,"order_id":"CN1515564337361","total":"2316","order_time":"2018-01-10 14:05","remark":"remark","cart":{"cart_id":5,"groups":[{"product_id":3,"name":"DG6086 Over-Molded Rubber Polarized 2809T3","image":"HPbanner/sunglasses/Dolce%20%26%20Gabbana%20DG6086%20Over-Molded%20Rubber%20Polarized_2809T3.jpg","price":1153,"quantity":2,"_id":"5a559c064d649a443e1219b9","extra":[],"attrs":[]}]},"__v":0}
]
```


### 25、获取订单信息

#### 请求URL:  
```
http://47.100.6.170:3389/v1/orders
```

#### 示例：
 [http://47.100.6.170:3389/v1/orders?order_id=6&user_id=2](http://47.100.6.170:3389/v1/orders?order_id=6&user_id=2)

#### 请求方式: 
```
get
```

#### 参数类型：query

|参数|是否必选|类型|说明|
|:-----|:-------:|:-----|:-----|
|user_id      |Y       |number  |用户id |
|order_id      |Y       |number  |订单id |

#### 返回示例：

```javascript
{
    "id":6,
    "user_id":2,
    "order_id":"CN1515564950885",
    "total":"2316",
    "order_time":"2018-01-10 14:15",
    "remark":"remark",
    "address":{"__v":0,"is_active":true,"address":"admin","email":"ga@dd.com","mobile":"127681902","gender":"men","name":"admin","user_id":2,"id":3},
    "cart":{
        "cart_id":5,
        "groups":[
            {
                "product_id":3,
                "name":"DG6086 Over-Molded Rubber Polarized 2809T3",
                "image":"HPbanner/sunglasses/Dolce%20%26%20Gabbana%20DG6086%20Over-Molded%20Rubber%20Polarized_2809T3.jpg",
                "price":1153,
                "quantity":2,
                "_id":"5a559c064d649a443e1219b9",
                "extra":[],
                "attrs":[]
            }
        ]
    },
    "__v":0
}
```

