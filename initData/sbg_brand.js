const brands = {
    'RECORDS': [
        {     
            "brand_id" : 2,
            "brand_name" : "Burberry",
            "brand_image" : "https://84654884b68f749e7aa3-aa57143e2ca30795bf94ff3884a3b19b.ssl.cf1.rackcdn.com/public/images/showbrand/brand_Burberry.gif",
        },
        {               
            "brand_id" : 3,
            "brand_name" : "Bvlgari",
            "brand_image" : "https://84654884b68f749e7aa3-aa57143e2ca30795bf94ff3884a3b19b.ssl.cf1.rackcdn.com/public/images/showbrand/brand_Bvlgari.gif",
        },
        {
            "brand_id" : 7,
            "brand_name" : "Dolce & Gabbana",
            "brand_image" : "https://84654884b68f749e7aa3-aa57143e2ca30795bf94ff3884a3b19b.ssl.cf1.rackcdn.com/public/images/showbrand/brand_Dolce%20%26%20Gabbana.gif",
        },
        {
            "brand_id" : 12,
            "brand_name" : "Miu Miu",
            "brand_image" : "https://84654884b68f749e7aa3-aa57143e2ca30795bf94ff3884a3b19b.ssl.cf1.rackcdn.com/public/images/showbrand/brand_Miu%20Miu.gif",
        },
        {
            "brand_id" : 10,
            "brand_name" : "Gucci",
            "brand_image" : "https://84654884b68f749e7aa3-aa57143e2ca30795bf94ff3884a3b19b.ssl.cf1.rackcdn.com/public/images/showbrand/brand_Gucci.gif",
        },
        {
            "brand_id" : 17,
            "brand_name" : "Versace",
            "brand_image" : "https://84654884b68f749e7aa3-aa57143e2ca30795bf94ff3884a3b19b.ssl.cf1.rackcdn.com/public/images/showbrand/brand_Versace.gif",
        },
        {
            "brand_id" : 13,
            "brand_name" : "Prada",
            "brand_image" : "https://84654884b68f749e7aa3-aa57143e2ca30795bf94ff3884a3b19b.ssl.cf1.rackcdn.com/public/images/showbrand/brand_Prada.gif",
        },
        {
            "brand_id" : 25,
            "brand_name" : "Police",
            "brand_image" : "https://84654884b68f749e7aa3-aa57143e2ca30795bf94ff3884a3b19b.ssl.cf1.rackcdn.com/public/images/showbrand/brand_Police.gif",
        },
        {
            "_id" : ObjectId("59e7340b566a5a54bac7aec9"),
            "brand_id" : 14,
            "brand_name" : "Ray Ban",
            "brand_image" : "https://84654884b68f749e7aa3-aa57143e2ca30795bf94ff3884a3b19b.ssl.cf1.rackcdn.com/public/images/showbrand/brand_Ray%20Ban.gif",
        },
        {
            "_id" : ObjectId("59e7340b566a5a54bac7aecb"),
            "brand_id" : 21,
            "brand_name" : "Adidas",
            "brand_image" : "https://84654884b68f749e7aa3-aa57143e2ca30795bf94ff3884a3b19b.ssl.cf1.rackcdn.com/public/images/showbrand/brand_Adidas.gif",
        },
        {
            "_id" : ObjectId("59e7340b566a5a54bac7aecf"),
            "brand_id" : 58,
            "brand_name" : "Tom Ford",
            "brand_image" : "https://84654884b68f749e7aa3-aa57143e2ca30795bf94ff3884a3b19b.ssl.cf1.rackcdn.com/public/images/showbrand/brand_Tom%20Ford.gif",
        },
        {
            "_id" : ObjectId("59e7340b566a5a54bac7aed0"),
            "brand_id" : 64,
            "brand_name" : "Porsche Design",
            "brand_image" : "https://84654884b68f749e7aa3-aa57143e2ca30795bf94ff3884a3b19b.ssl.cf1.rackcdn.com/public/images/showbrand/brand_Porsche%20Design.gif",
        }
    ]
}
export default brands.RECORDS;