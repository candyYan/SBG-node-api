const category = {
"RECORDS":[
{
"category_id":"1",
"category_name":"Sunglasses",
"parent_id":"0",
"var_category_name":"sunglasses",
"category_url":"designer-sunglasses",
"duration":""
},
{
"category_id":"2",
"category_name":"Eyeglasses",
"parent_id":"0",
"var_category_name":"eyeglasses",
"category_url":"designer-eyeglasses",
"duration":""
},
{
"category_id":"3",
"category_name":"Contact Lens",
"parent_id":"0",
"var_category_name":"contact_lenses",
"category_url":"contact-lenses",
"duration":""
},
{
"category_id":"4",
"category_name":"Daily Disposable",
"parent_id":"3",
"var_category_name":"daily-disposable",
"category_url":"daily-disposable",
"duration":""
},
{
"category_id":"5",
"category_name":"1-2 weeks Disposable",
"parent_id":"3",
"var_category_name":"1-2-weeks-disposable",
"category_url":"1-2-weeks-disposable",
"duration":""
},
{
"category_id":"6",
"category_name":"semi-monthly disposable",
"parent_id":"3",
"var_category_name":"semi_monthlies",
"category_url":"semi-monthly-disposable",
"duration":""
},
{
"category_id":"7",
"category_name":"Monthly Disposable",
"parent_id":"3",
"var_category_name":"monthly-disposable",
"category_url":"monthly-disposable",
"duration":""
},
{
"category_id":"8",
"category_name":"Yearly Disposable",
"parent_id":"3",
"var_category_name":"yearlies",
"category_url":"yearly-disposable",
"duration":""
},
{
"category_id":"9",
"category_name":"three-monthly disposable",
"parent_id":"3",
"var_category_name":"three-monthly",
"category_url":"three-monthly disposable",
"duration":""
},
{
"category_id":"10",
"category_name":"six-monthly disposable",
"parent_id":"3",
"var_category_name":"six-monthly",
"category_url":"six-monthly disposable",
"duration":""
},
{
"category_id":"20",
"category_name":"Eye Care Solution",
"parent_id":"3",
"var_category_name":"eye_care_solution",
"category_url":"eye-care-solution-contact-lenses",
"duration":""
},
{
"category_id":"21",
"category_name":"Comfort Drops",
"parent_id":"3",
"var_category_name":"comfort_drops",
"category_url":"comfort-drops-contact-lenses",
"duration":""
}
]
};

export default category.RECORDS;