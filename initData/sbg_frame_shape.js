const frame = {
"RECORDS":[
{
"frame_shape_id":"1",
"frame_shape_name":"Single Lens"
},
{
"frame_shape_id":"2",
"frame_shape_name":"Aviator"
},
{
"frame_shape_id":"3",
"frame_shape_name":"Wayfarer"
},
{
"frame_shape_id":"4",
"frame_shape_name":"Rectangle"
},
{
"frame_shape_id":"5",
"frame_shape_name":"Oval"
},
{
"frame_shape_id":"6",
"frame_shape_name":"Round"
},
{
"frame_shape_id":"7",
"frame_shape_name":"Oversized"
},
{
"frame_shape_id":"9",
"frame_shape_name":"Wraparound"
},
{
"frame_shape_id":"10",
"frame_shape_name":"Butterfly"
},
{
"frame_shape_id":"11",
"frame_shape_name":"Cat Eye"
},
{
"frame_shape_id":"12",
"frame_shape_name":"Goggle"
}
]
}

export default frame.RECORDS