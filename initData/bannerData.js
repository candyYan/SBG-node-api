/**
 * Description
 *
 * Creation Date 18/10/2017
 * @author Candy
 */

export default [
    {
        banner_id: 1,
        banner_url: "https://84654884b68f749e7aa3-aa57143e2ca30795bf94ff3884a3b19b.ssl.cf1.rackcdn.com/en/images/banners/ImgSlider-Mobile-CL-EN.jpg",
        banner_link: "https://smartbuyglasses.tmall.hk/",
        banner_time: "2017-12-14 24:59:59"
    },
    {
        banner_id: 2,
        banner_url: "https://84654884b68f749e7aa3-aa57143e2ca30795bf94ff3884a3b19b.ssl.cf1.rackcdn.com/en/images/banners/HP-Mobile-NH-11.jpg",
        banner_link: "https://smartbuyglasses.tmall.hk/category-1307936441.htm?spm=a1z10.1-b-s.w5003-16486414935.36.ws74Hp&search=y&tsearch=y&scene=taobao_shop#TmshopSrchNav",
        banner_time: "2017-12-14 24:59:59"
    }
]
