export default [
  {
    product_id: 1,
    category_id: 1,
    brand_id: 14,
    gender_id: 2,
    product_name : "RB4165 Justin 852/88",
    price: 700,
    product_image: "HPbanner/sunglasses/Ray%20Ban%20RB%204165%20852_88.jpg"
  },
    {
        product_id: 2,
        category_id: 1,
        brand_id: 2,
        gender_id: 2,
        product_name : "GRAND PRIX 2 T4M/9O",
        price: 651,
        product_image: "HPbanner/sunglasses/Carrera%20GRAND%20PRIX%202T4M_9O.jpg"
    },
    {
        product_id: 3,
        category_id: 1,
        brand_id: 2,
        gender_id: 4,
        product_name : "DG6086 Over-Molded Rubber Polarized 2809T3",
        price: 1153,
        product_image: "HPbanner/sunglasses/Dolce%20%26%20Gabbana%20DG6086%20Over-Molded%20Rubber%20Polarized_2809T3.jpg"
    },
    {
        product_id: 4,
        category_id: 2,
        brand_id: 14,
        gender_id: 3,
        product_name : "RX5228 Highstreet 2000",
        price: 801,
        product_image: "HPbanner/glasses/Ray_Ban_RB5228_2000.jpg"
    },
    {
        product_id: 5,
        category_id: 2,
        brand_id: 2,
        gender_id: 3,
        product_name : "FT5294 052",
        price: 1153,
        product_image: "HPbanner/glasses/Tom%20Ford%20FT5294%20052.jpg"
    }
]
