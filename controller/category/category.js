/**
 * Description
 *
 * Creation Date 12/10/2017
 * @author Candy
 */
import BaseComponent from '../../prototype/baseComponent'
import CategoryModel from '../../models/category/category'
import BrandModel from  '../../models/category/brand'
import FrameModel from  '../../models/category/frame'
import GenderModer from '../../models/category/gender'

class Category extends BaseComponent {
  constructor() {
    super();
  }

  async getCategoryByType(req, res, next) {
    const type = req.query.type;
    let categoryInfo;
    try {
      switch(type) {
        case 'category':
          categoryInfo = await CategoryModel.find({}, '-_id').sort({category_id: 1});
          break;
        case 'brand':
          const {limit = 12, offset = 0} = req.query
          categoryInfo = await BrandModel.find({}, '-_id').sort({total_sales: -1}).limit(limit).skip(offset);
          break;
        case 'frame':
          categoryInfo = await FrameModel.find({}, '-_id').sort({frame_type_id: 1});
          break;
        case 'gender':
          categoryInfo = await GenderModer.find({}, '-_id').sort({gender_id: 1});
          break;
        default:
          res.json({
            name: 'ERROR_QUERY_TYPE',
            message: 'Param Error',
          })
          return
      }
      res.send(categoryInfo)
    } catch (err) {
      console.log(err)
      res.send({
        status: 0,
        message: 'get category fail'
      })
    }
  }


}

export default new Category();
