/**
 * Description
 *
 * Creation Date 24/10/2017
 * @author Candy
 */
import BaseComponent from '../../prototype/baseComponent'
import AddressModel from '../../models/v1/address'
import formidable from 'formidable'

class Address extends BaseComponent {
    constructor() {
        super()
        this.addAddress = this.addAddress.bind(this)
    }
    async addAddress(req, res, next) {
        const form = new formidable.IncomingForm()
        form.parse(req, async(err, fileds, files) => {
            const user_id = req.params.user_id
            const { name, gender = 'men', mobile, email, address, is_active = false} = fileds
            try {
                if(!name) {
                    throw new Error('Name is null!')
                } else if (!mobile) {
                    throw new Error('Mobile is null!')
                } else if (!email) {
                    throw new Error('Email is null!')
                } else if (!address) {
                    throw new Error('Address is null!')
                } else if (!gender) {
                    throw new Error('Gender is null!')
                }
            } catch (err) {
                console.log(err.message);
                res.send({
                    status: 0,
                    type: 'GET_WRONG_PARAM',
                    message: err.message
                })
                return
            }
            try {
                if(is_active) {
                    await AddressModel.findOneAndUpdate({is_active: true}, {$set: { is_active: false}})
                }
                const address_id = await this.getId('address_id')
                const newAddress = {
                    id: address_id,
                    user_id,
                    name,
                    gender,
                    mobile,
                    email,
                    address,
                    is_active
                }
                await AddressModel.create(newAddress)
                res.send({
                    status: 1,
                    message: 'Add address successful!'
                })
            } catch (err) {
                console.log('Add address fail', err)
                res.send({
                    status: 0,
                    message: 'Add address fail!'
                })
            }
        })
    }
    async updateAddress(req, res, next) {
        const {user_id, address_id} = req.params
        if(!user_id || !Number(user_id) || !address_id || !Number(address_id)) {
            res.send({
                type: 'ERROR_QUERY',
                message: 'Query Param is wrong'
            })
        }
        const form = new formidable.IncomingForm()
        form.parse(req, async(err, fileds, files) => {
            const { name, gender, mobile, email, address, is_active = false} = fileds
            try {
                if(!name) {
                    throw new Error('Name is null!')
                } else if (!mobile) {
                    throw new Error('Mobile is null!')
                } else if (!email) {
                    throw new Error('Email is null!')
                } else if (!address) {
                    throw new Error('Address is null!')
                } else if (!gender) {
                    throw new Error('Gender is null!')
                }
            } catch (err) {
                console.log(err.message);
                res.send({
                    status: 0,
                    type: 'GET_WRONG_PARAM',
                    message: err.message
                })
                return
            }
            try {
                const newAddress = {name, gender, mobile, email, address, is_active}
                if(is_active) {
                    await AddressModel.findOneAndUpdate({is_active: true}, {$set: { is_active: false}})
                }
                await AddressModel.findOneAndUpdate({user_id, id: address_id}, {$set: newAddress})
                res.send({
                    status: 1,
                    message: 'Add address successful!'
                })
            } catch (err) {
                console.log('Add address fail', err)
                res.send({
                    status: 0,
                    message: 'Add address fail!'
                })
            }
        })
    }
    async getAddress(req, res, next) {
        const user_id = req.params.user_id
        if(!user_id && !Number(user_id)) {
            res.send({
                type: 'ERROR_QUERY',
                message: 'Query Param is wrong'
            })
        }
        try {
            const addresses = await AddressModel.find({user_id}, '-_id').sort({id: -1})
            res.send(addresses)
        } catch (err) {
            res.send({
                type: 'ERROR_GET_ADDRESS',
                message: err
            })
        }
    }

    async deleteAddress(req, res, next) {
        const {user_id, address_id} = req.params
        if(!user_id || !Number(user_id) || !address_id || !Number(address_id)) {
            res.send({
                type: 'ERROR_QUERY',
                message: 'Query Param is wrong'
            })
        }
        try {
            await AddressModel.findOneAndRemove({user_id, id: address_id})
            res.send({
                status: 1,
                message: 'Delete Address Successful!',
            })
        } catch (err) {
            res.send({
                status: 0,
                type: 'ERROR_DELETE_ADDRESS',
                message: err
            })
        }
    }

    async getAddressById(req, res, next) {
        const {user_id, address_id} = req.params;
        if(!user_id || !Number(user_id) || !address_id || !Number(address_id)) {
            res.send({
                type: 'ERROR_QUERY',
                message: 'Query Param is wrong'
            })
        }
        try {
            const addresses = await AddressModel.findOne({user_id, id: address_id}, '-_id')
            res.send(addresses)
        } catch (err) {
            res.send({
                type: 'ERROR_GET_ADDRESS',
                message: err
            })
        }
    }
}

export default new Address()