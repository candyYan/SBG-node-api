/**
 * Description
 *
 * Creation Date 18/10/2017
 * @author Candy
 */
import captchaspng from 'captchapng'

class Captchas {
    constructor() {}

    async getCaptchas(req, res, next) {
        const cap = parseInt(Math.random()*9000 + 1000)
        const p = new captchaspng(80, 30, cap)
        p.color(0, 0, 0, 0)
        p.color(80, 80, 80, 255)
        const base64 = p.getBase64()
        res.cookie('cap', cap, {maxAge: 300000, httpOnly: true})
        res.send({
            status: 1,
            code: 'data:image/png;base64,' + base64
        })
    }
}

export default new Captchas()
