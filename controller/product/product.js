/**
 * Description
 *
 * Creation Date 12/10/2017
 * @author Candy
 */
import Basecomponent from '../../prototype/baseComponent'
import productGlassesModel from '../../models/product/product_glasses'
import brandModel from '../../models/category/brand'
import productGlassesColorModel from '../../models/product/product_glasses_color'
import productPriceModel from '../../models/product/product_price'
import productSizeModel from '../../models/product/product_size'
import productReviewModel from '../../models/product/product_reviews'

class Product extends Basecomponent {
  constructor() {
    super()
  }
  async getProduct(req, res, next) {
      const {limit = 20, offset = 0, keyword, brand_id, category_id, gender_id, sort} = req.query;
      try {
          let productGlassesList;
          if(brand_id) {
              productGlassesList = await productGlassesModel.find({brand_id}, '-_id').sort({product_id: 1}).limit(Number(limit)).skip(Number(offset));
          } else if (category_id) {
              productGlassesList = await productGlassesModel.find({category_id}, '-_id').sort({product_id: 1}).limit(Number(limit)).skip(Number(offset));
          } else if (gender_id) {
              productGlassesList = await productGlassesModel.find({gender_id}, '-_id').sort({product_id: 1}).limit(Number(limit)).skip(Number(offset));
          } else if (keyword) {
              productGlassesList = await productGlassesModel.find({keyword}, '-_id').sort({product_id: 1}).limit(Number(limit)).skip(Number(offset));
          } else if(sort == '1') {
              productGlassesList = await productGlassesModel.find({}, '-_id').sort({price: 1}).limit(Number(limit)).skip(Number(offset));
          } else if(sort == '0') {
              productGlassesList = await productGlassesModel.find({}, '-_id').sort({price: -1}).limit(Number(limit)).skip(Number(offset));
          } else if (!brand_id || !category_id || !gender_id || !keyword || !sort) {
              productGlassesList = await productGlassesModel.find({}, '-_id').sort({product_id: 1}).limit(Number(limit)).skip(Number(offset));
          }


          if(!productGlassesList) {
              console.log('Get products fail!')
              res.send({
                  static: 0,
                  message: 'Get products fail!'
              })
              return
          }
          res.send(productGlassesList)
      } catch (err) {
          res.send({
              static: 0,
              message: 'Get products fail!'
          })
      }
  }
  async getProdutById(req, res, next) {
    const product_id = req.params.product_id
    if(!product_id) {
      throw new Error('Param Error!')
      return
    }
    try {
      const productGlass = await productGlassesModel.findOne({product_id}, '-_id')
      if(!productGlass) {
        console.log('Get product fail!')
        res.send({
          static: 0,
          message: 'Get products fail!'
        })
        return
      }
      res.send(productGlass)

    } catch (err) {
      res.send({
        static: 0,
        message: 'Get product fail!'
      })
    }
  }

  async getSingleProductPriceAndSize(req, res, next) {
    let color_code_id = req.params.color_code_id;
    if(!color_code_id || !Number(color_code_id)) {
      throw new Error('Param Error!')
      return
    }
    try {
      const price = await productPriceModel.findOne({color_code_id}, '-_id');
      const size = await productSizeModel.findOne({color_code_id}, '-_id');
      const review = await productReviewModel.findOne({color_code_id}, '-_id');
      if(!price || !size) {
        console.log('Get product Price fail!')
        res.send({
          static: 0,
          message: 'Get product Price fail!'
        })
        return
      }
      res.send({
        price,
        size,
        review
      })
    } catch (err) {
      console.log(err)
      res.send({
        static: 0,
        message: 'Get product Price fail!'
      })
    }
  }


}

export default new Product();