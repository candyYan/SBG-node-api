/**
 * Description
 *
 * Creation Date 18/10/2017
 * @author Candy
 */

import homeBannerModel from  '../../models/v3/homeBanner'

class Banner {
    constructor() {
    }
    async getBanner(req, res, next) {
        try {
            const bannerInfo = await homeBannerModel.find();
            res.send(bannerInfo)
        } catch (err) {
            console.log('Api record error', err)
            throw new Error('Api record error')
        }
        next()
    }
}

export default new Banner();