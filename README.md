

>  开发环境 macOS 10.12.6 nodejs v8.1.2  Mongodb v3.4.6


## 技术栈

nodejs + express + mongodb + mongoose + es6/7 


## 项目运行

```
项目运行之前，请确保系统已经安装以下应用
1、开发环境 macOS 10.12.6 nodejs v8.1.2
2、mongodb (开启状态)
```

```

npm install

npm run dev


```
# API接口文档

## [接口文档地址](https://gitee.com/candyYan/SBG-node-api/blob/master/API.md)









