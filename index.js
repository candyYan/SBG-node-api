/**
 * Use ES6/ES7 by babel-register.js
 *
 * Creation Date 12/10/2017
 * @author Candy
 */
'use strict';

require('babel-register');
require('./app');