/**
 * Configuration
 *
 * Creation Date 12/10/2017
 * @author Candy
 */

export default {
  port: 3389,
  url: 'mongodb://candy:123456@127.0.0.1:27017/SBGDatabase',
  session: {
    name: 'SID',
    secret: 'SID',
    cookie: {
      httpOnly: true,
      secure: false,
      maAge:  365 * 24 * 60 * 60 * 1000,
    }
  }
}