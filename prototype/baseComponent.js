/**
 * Base Component prototype
 *
 * Creation Date 12/10/2017
 * @author Candy
 */
'use strict';
import IdsModel from '../models/ids'

class BaseComponent {
  constructor() {
    this.idList = ['category_id', 'address_id', 'frame_type_id', 'gender_id', 'gender_id', 'gender_id', 'product_id', 'color_code_id', 'brand_id', 'cart_id', 'order_id', 'user_id', 'admin_id', 'statis_id']
  }

  //Get Id List
  async getId(type) {
    if(!this.idList.includes(type)) {
      console.log('Id type error');
      throw new Error('Id type error');
      return
    }
    try {
      const idData = await IdsModel.findOne();
      if(!idData) {
        console.log('Get id date fail')
        return
      }
      idData[type]++;
      await idData.save();
      return idData[type];
    } catch (err) {
      console.log('Get id date fail')
      throw new Error(err);
    }
  }
}

export default BaseComponent
