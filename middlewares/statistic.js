/**
 * Description
 *
 * Creation Date 12/10/2017
 * @author Candy
 */
import BaseComponent from '../prototype/baseComponent'
import StatisModel from  '../models/statis/statis'
import dtime from 'time-formater'

class Statistic extends BaseComponent {
  constructor() {
    super();
    this.apiRecord = this.apiRecord.bind(this)
  }
  async apiRecord(req, res, next) {
    try {
      const statis_id = await this.getId('statis_id');
      if(!statis_id) {
        console.log(err)
        return
      }
      let date = dtime().format('YYYY-MM-DD HH:mm:ss'),
        origin = req.originalUrl;
      const apiInfo = {
        statis_id,
        date,
        origin
      }
      StatisModel.create(apiInfo)
    } catch (err) {
      console.log('Api record error', err)
      throw new Error('Api record error')
    }
    next()
  }
}

export default new Statistic();