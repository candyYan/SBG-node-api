/**
 * Connect Mongodb database
 *
 * Creation Date 12/10/2017
 * @author Candy
 */

'use strict';
import mongoose from 'mongoose';
import config from '../config/defalut'

mongoose.connect(config.url, {useMongoClient: true},);
// mongoose.connect(config.url);
mongoose.Promise = global.Promise;

const db = mongoose.connection;

db.once('open', () => {
  console.log('Connect database Successfully!');
});

db.on('error', (err) => {
  console.log('Error in MongoDb connection: ' + err);
  mongoose.disconnect();
});

db.on('close', () => {
  console.log('Database disconnect, reconnect database!');
  mongoose.connect(config.url, {server: {auto_reconnect: true}});
});